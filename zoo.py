#!/usr/bin/env python3

import os
import itertools
import sys
import enum

# Input
import yaml

#####
# https://gist.github.com/pypt/94d747fe5180851196eb
# Fixes https://github.com/yaml/pyyaml/issues/41
class UniqueKeyLoader(yaml.SafeLoader):
    def construct_mapping(self, node, deep=False):
        mapping = []
        for key_node, value_node in node.value:
            key = self.construct_object(key_node, deep=deep)
            if key in mapping:
                raise ValueError("Duplicate key " + key)
            mapping.append(key)
        return super().construct_mapping(node, deep)
#####

# Processing
import networkx as nx

# Output
import re
import jinja2
from networkx.drawing.nx_agraph import to_agraph

# DBLP
import urllib.request
import bs4


# Vertices are proof systems (duplicated) and formulas.
# Edge from A to B if A simulates B
# Edge from A' to B' if A simulates B
# Edge from A to F if A can prove F efficiently
# Edge from F to A' if A cannot prove F efficiently
Gsep = nx.OrderedDiGraph()

with open('zoo.yaml','r') as f:
    data = yaml.load(f, UniqueKeyLoader)

print(yaml.dump(data),file=sys.stderr)

proofsystems = data["proofsystems"]
for ps in proofsystems:
    Gsep.add_node(ps)
    Gsep.add_node(ps+"_")

formulas = data["formulas"]
for f in formulas:
    Gsep.add_node(f)

def dblp(v):
    key=v["dblp"]
    url=f"http://dblp.uni-trier.de/rec/bibtex/{key}.xml"
    v["url"]=f"http://dblp.uni-trier.de/rec/{key}.html"
    localkey=key.translate(str.maketrans('/','_'))
    filename=f"cache/{localkey}.xml"
    if not os.path.isfile(filename):
        urllib.request.urlretrieve(url, filename)
    with open(filename) as f:
        bib = bs4.BeautifulSoup(f, "html.parser")
        v["title"] = bib.title.string
    print(v,file=sys.stderr)
    return v

papers = data["papers"]
for p,v in papers.items():
    if "dblp" in v:
        papers[p]=dblp(v)
for p,v in papers.items():
    print(v,file=sys.stderr)

overheadvalues = {
    "none": 0,
    "poly": 0,
    "quasipoly": 1,
}
sizevalues = {
    "poly": 0,
    "quasipoly": 1,
    "exp": 1000,
}

def checkps(ps):
    if ps not in proofsystems:
        raise ValueError(f"{ps} is not a proof system id, check for typos or add a new proof system")

def checkf(f):
    if f not in formulas:
        raise ValueError(f"{f} is not a formula id, check for typos or add a new formula")

def checkpaper(p):
    if p not in papers:
        raise ValueError(f"{p} is not a paper id, check for typos or add a new paper")

def ensurelist(l):
    if isinstance(l, list):
        return l
    elif l is None:
        return []
    else:
        return [l]

def listorscalar(m, name):
    if name in m:
        l=m[name]
        return ensurelist(m[name])
    return []


subsystems = {ps : set() for ps in proofsystems}
for weak,z in proofsystems.items():
    for strong in listorscalar(z,'subsystemof'):
        checkps(strong)
        checkps(weak)
        subsystems[strong].add(weak)
        Gsep.add_edge(strong, weak, weight=0)
        Gsep.add_edge(strong+"_", weak+"_", weight=0)

for weak,z in formulas.items():
    for strong in listorscalar(z,'extends'):
        checkf(strong)
        checkf(weak)
        Gsep.add_edge(strong, weak, weight=0)

simulations = data["simulations"]
for strong,z in simulations.items():
    for weak,s in z.items():
        checkps(strong)
        checkps(weak)
        overhead = s.get("overhead", "poly")
        v = overheadvalues[overhead]
        Gsep.add_edge(strong, weak, weight=v)
        Gsep.add_edge(strong+"_", weak+"_", weight=v)

upperbounds = data["upperbounds"]
for ps,z in upperbounds.items():
    if isinstance(z,str):
        upperbounds[ps] = z = {z: {}}
    for f,ub in z.items():
        checkps(ps)
        checkf(f)
        if ub is None:
            upperbounds[ps][f] = ub = {}
        size = ub.get("size", "poly")
        v = sizevalues[size]
        Gsep.add_edge(ps, f, weight=v)

lowerbounds = data["lowerbounds"]
for ps,z in lowerbounds.items():
    if isinstance(z,str):
        lowerbounds[ps] = z = {z: {}}
    for f,lb in z.items():
        checkps(ps)
        checkf(f)
        if lb is None:
            lowerbounds[ps][f] = lb = {}
        size = lb["size"]
        v = sizevalues[size]
        Gsep.add_edge(f, ps+"_", weight=-v)

reach = dict(nx.all_pairs_bellman_ford_path_length(Gsep))
reachpath = dict(nx.all_pairs_bellman_ford_path(Gsep))
#print(reach, file=sys.stderr)

Relation = enum.Enum('Relation', 'EQUIV STRONGER WEAKER INCOMP SIMS SIMD NSIMS NSIMD UNREL')
rel_to_string = {
    Relation.EQUIV: "equivalent",
    Relation.STRONGER: "stronger than",
    Relation.WEAKER: "weaker than",
    Relation.INCOMP: "incomparable wrt",
    Relation.SIMS: "simulates",
    Relation.SIMD: "simulated by",
    Relation.NSIMS: "does not simulate",
    Relation.NSIMD: "not simulated by",
    Relation.UNREL: "[missing?]",
}

def relation_id(ps1, ps2):
    sim12 = ps2 in reach[ps1]
    sim21 = ps1 in reach[ps2]
    sep12 = (reach[ps1].get(ps2+"_",0))<0
    sep21 = (reach[ps2].get(ps1+"_",0))<0
    #print(ps1,ps2,sim12,sim21,sep12,sep21, file=sys.stderr)
    if sim12 and sim21 : return Relation.EQUIV
    elif sim12 and sep12 : return Relation.STRONGER
    elif sim21 and sep21 : return Relation.WEAKER
    elif sep12 and sep21 : return Relation.INCOMP
    elif sim12 : return Relation.SIMS
    elif sim21 : return Relation.SIMD
    elif sep21 : return Relation.NSIMS
    elif sep12 : return Relation.NSIMD
    else: return Relation.UNREL

def relation_natural(ps1, ps2):
    sim12 = ps2 in sims[ps1]
    sim21 = ps1 in sims[ps2]
    sep12 = (reach[ps1].get(ps2+"_",0))<0
    sep21 = (reach[ps2].get(ps1+"_",0))<0
    name1 = proofsystems[ps1]['longname']
    name2 = proofsystems[ps2]['longname']
    #print(ps1,ps2,sim12,sim21,sep12,sep21, file=sys.stderr)
    if sim12 and sim21 :
        return f"{name1} and {name2} equivalent"
    elif sim12 and sep12 :
        return f"{name1} stronger than {name2}"
    elif sim21 and sep21 :
        return f"{name2} stronger than {name1}"
    elif sep12 and sep21 :
        return f"{name1} and {name2} incomparable"
    elif sim12 :
        return f"{name1} simulates {name2}"
    elif sim21 :
        return f"{name2} simulates {name1}"
    elif sep12 :
        return f"{name2} does not simulate {name1}"
    elif sep21 :
        return f"{name1} does not simulate {name2}"
    else:
        return f"{name1} and {name2} unrelated"

def complexity_id(ps, f):
    if f in reach[ps]:
        #print(f"{ps} {f} {reach[ps][f]}")
        if reach[ps][f]==0 : return "polynomial"
        if reach[ps][f]>0 : return "quasipolynomial"
    if ps+"_" in reach[f]:
        #print(f"{ps} {f} {reach[f][ps+'_']}")
        if reach[f][ps+"_"]<0 : return "exponential"
    return "[missing?]"

def format_as_path(l):
    return " → ".join(l)

def get_sources(d):
    sources = listorscalar(d, "source")
    for s in sources:
        checkpaper(s)
    if (sources):
        return sources
    else:
        return "[citation needed]"

def complexity_source(ps, f):
    if ps in lowerbounds:
        if f in lowerbounds[ps]:
            return get_sources(lowerbounds[ps][f])
    if ps in upperbounds:
        if f in upperbounds[ps]:
            return get_sources(upperbounds[ps][f])
    if f in reachpath[ps]:
        return format_as_path(reachpath[ps][f])
    if ps+"_" in reachpath[f]:
        return format_as_path(reachpath[f][ps+"_"])
    return []

def relation_source_ub(ps1, ps2):
    if ps1 in simulations:
        if ps2 in simulations[ps1]:
            return get_sources(simulations[ps1][ps2])
    if ps2 in simulations:
        if ps1 in simulations[ps2]:
            return get_sources(simulations[ps2][ps1])
    if ps2 in subsystems[ps1] or ps1 in subsystems[ps2]:
        return "[subsystem]"
    if ps2 in reachpath[ps1]:
        return format_as_path(reachpath[ps1][ps2])
    if ps1 in reachpath[ps2]:
        return format_as_path(reachpath[ps2][ps1])
    return None

def relation_source_lb(ps1, ps2):
    sources = []
    if ps2+"_" in reachpath[ps1]:
        sources.append(format_as_path(reachpath[ps1][ps2+"_"]))
    if ps1+"_" in reachpath[ps2]:
        sources.append(format_as_path(reachpath[ps2][ps1+"_"]))
    return sources

def relation_source(ps1, ps2):
    ub = ensurelist(relation_source_ub(ps1, ps2))
    lb = ensurelist(relation_source_lb(ps1, ps2))
    return ub + lb

def complexity(ps, f):
    ret = {"ps": ps,
            "f": f,
            "id": complexity_id(ps, f)
           }
    source = complexity_source(ps, f)
    if source:
        ret["source"]=ensurelist(source)
    return ret

def complexity_natural(ps, f):
    return f"The complexity of {f} in {ps} is {cplex}"

def relation(ps1, ps2):
    ret = {"ps1": ps1,
           "ps2": ps2,
           "id": relation_id(ps1, ps2),
           "str": rel_to_string[relation_id(ps1, ps2)]
           }
    source = relation_source(ps1, ps2)
    if source:
        ret["source"]=ensurelist(source)
    return ret


#for ps1, ps2 in itertools.combinations(proofsystems,2):
#    print(relation(ps1, ps2))

templateLoader = jinja2.FileSystemLoader(searchpath="./")
templateEnv = jinja2.Environment(loader=templateLoader)
# https://stackoverflow.com/questions/12791216/how-do-i-use-regular-expressions-in-jinja2
def strip_svg_header(s):
    return re.sub('<[?]xml.*?[?]>|<!DOCTYPE.*?>', '', s, flags=re.DOTALL)

def visible_dots(s):
    return re.sub('stroke-dasharray="1,5"', 'stroke-dasharray="1,2"', s)

templateEnv.filters['strip_svg_header'] = strip_svg_header
templateEnv.filters['visible_dots'] = visible_dots
indexTemplate = templateEnv.get_template("index.html")
proofsystemTemplate = templateEnv.get_template("proofsystem.html")
formulaTemplate = templateEnv.get_template("formula.html")

def all_about_ps(ps1):
    relations = [relation(ps1, ps2) for ps2 in proofsystems if ps1 != ps2]
    complexities = [complexity(ps1, f) for f in formulas]
    with open(f"build/{ps1}.html", "w") as f:
        print(proofsystemTemplate.render(
            ps1=ps1,
            relations=relations,
            complexities=complexities,
            proofsystems=proofsystems,
            formulas=formulas,
            papers=papers), file=f)

for ps1 in proofsystems:
    all_about_ps(ps1)

def all_about_formula(ff):
    complexities = [complexity(ps, ff) for ps in proofsystems]
    with open(f"build/{ff}.html", "w") as f:
        print(formulaTemplate.render(
            ff=ff,
            complexities=complexities,
            proofsystems=proofsystems,
            formulas=formulas,
            papers=papers), file=f)

for ff in formulas:
    all_about_formula(ff)

def index():
    with open("build/index.html", "w") as f:
        print(indexTemplate.render(
            proofsystems=proofsystems,
            formulas=formulas,
            papers=papers), file=f)

def raw_plot():
    H = to_agraph(Gsep)

    H.graph_attr["forcelabels"]=True
    H.graph_attr["newrank"]=True
    H.graph_attr["splines"]="polyline"

    H.write("zoo-raw.dot")
    os.system("dot -T pdf zoo-raw.dot -o build/zoo-raw.pdf")

def clean_plot(minimal=False):
    Gclean = nx.OrderedDiGraph()
    equivalences = []
    for ps in proofsystems:
        Gclean.add_node(ps,style="filled",fillcolor="#f7f7f7",penwidth="0")

    def equivalence(ps1, ps2):
        Gclean.add_edge(ps1,ps2,dir="both",constraint=False,color="#222288")
        equivalences.append((ps1,ps2))

    def simsep(ps1, ps2):
        Gclean.add_edge(ps1,ps2,style="dashed",color="#882288")

    def osim(ps1, ps2):
        Gclean.add_edge(ps1,ps2,style="solid",color="#222222")

    def osep(ps1, ps2):
        Gclean.add_edge(ps1,ps2,constraint=False,style="dotted",color="#882222")

    def sepsep(ps1, ps2):
        Gclean.add_edge(ps1,ps2,constraint=False,dir="both",style="dotted",color="#aa6622")

    rel_to_edge = {
        Relation.EQUIV: lambda ps1, ps2: equivalence(ps1,ps2),
        Relation.STRONGER: lambda ps1, ps2: simsep(ps1,ps2),
        Relation.WEAKER: lambda ps1, ps2: simsep(ps2,ps1),
        Relation.INCOMP: lambda ps1, ps2: sepsep(ps2,ps1),
        Relation.SIMS: lambda ps1, ps2: osim(ps1,ps2),
        Relation.SIMD: lambda ps1, ps2: osim(ps2,ps1),
        Relation.NSIMS: lambda ps1, ps2: osep(ps2,ps1),
        Relation.NSIMD: lambda ps1, ps2: osep(ps1,ps2),
        Relation.UNREL: lambda ps1, ps2: None,
    }

    Grel = Gsep.copy()
    Grel.remove_nodes_from(formulas)
    Gsep_allformulas = Gsep.copy()
    for ps in proofsystems:
        for f in formulas:
            if f in reach[ps]:
                Gsep_allformulas.add_edge(ps,f)
    directreach = dict(nx.all_pairs_shortest_path_length(Gsep_allformulas, cutoff=2))
    for ps1, ps2 in itertools.product(proofsystems, repeat=2):
        if ps2+"_" in directreach[ps1]:
            Grel.add_edge(ps1,ps2+"_")

    Gcond = nx.algorithms.components.condensation(Grel)
    scc = Gcond.graph['mapping']
    classes = {}
    for k,v in scc.items():
        classes.setdefault(v, []).append(k)
    canonical = {}
    for k,v in classes.items():
        v = sorted(v,key=len)
        for i,ps in enumerate(v):
            canonical[ps]=(i==0)
    Gtrans = nx.algorithms.dag.transitive_reduction(Gcond)

    for ps1, ps2 in itertools.combinations(proofsystems, 2):
        if scc[ps1]==scc[ps2]:
            if canonical[ps1] or canonical[ps2]:
                equivalence(ps1,ps2)
        if not canonical[ps1] or not canonical[ps2]:
            continue
        scc1=scc[ps1]
        scc2=scc[ps2]
        scc1_=scc[ps1+"_"]
        scc2_=scc[ps2+"_"]
        sim = scc2 in Gtrans[scc1] or scc1 in Gtrans[scc2]
        sep = scc2_ in Gtrans[scc1] or scc1_ in Gtrans[scc2]
        if sim or sep:
            rel_to_edge[relation_id(ps1,ps2)](ps1,ps2)

    urls = {ps: ps+".html" for ps in proofsystems}
    longnames = {ps: proofsystems[ps]['longname'] for ps in proofsystems}
    nx.set_node_attributes(Gclean, urls, "URL")
    nx.set_node_attributes(Gclean, longnames, "tooltip")

    H = to_agraph(Gclean)
    H.graph_attr["forcelabels"]=True
    H.graph_attr["newrank"]=True
    H.graph_attr["splines"]="spline"

    for eq in equivalences:
        H.add_subgraph(eq,rank='same')

    H.graph_attr["label"]="Proof Complexity Zoo - https://proofcomplexityzoo.gitlab.io/zoo/"
    H.graph_attr["labelloc"]="b"
    H.graph_attr["labeljust"]="r"

    H.write("zoo-clean.dot")
    os.system("dot -T pdf zoo-clean.dot -o build/zoo-clean.pdf")
    os.system("dot -T svg zoo-clean.dot -o build/zoo-clean.svg")

def produce_output():
    raw_plot()
    clean_plot()
    index()

if __name__ == '__main__':
    produce_output()
