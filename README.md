# Proof Complexity Zoo

Inspired by the [Complexity
Zoo](https://complexityzoo.net/Complexity_Zoo) and more precisely by
its automated counterpart the [Complexity
Zoology](https://www.math.ucdavis.edu/~greg/zoology/intro.html), the
proof complexity zoo is a community-editable database of proof
systems, formulas, and relations among them. It can be browsed at
https://proofcomplexityzoo.gitlab.io/zoo/.

Right now the project is just a proof of concept, and suggestions on
which data to store, which queries to support, and how to present the
information are particularly welcome. The database can be edited at
https://gitlab.com/proofcomplexityzoo/zoo/-/edit/master/zoo.yaml after
creating a user account and asking for permissions. Suggestions on how
to make editing more seamless are also welcome. Of course, patches are
even better!

## How to Edit

The database is a simple text file that can be edited at
https://gitlab.com/proofcomplexityzoo/zoo/-/edit/master/zoo.yaml. It
should be reasonably intuitive from context, but here is some
explanation about how it is structured.

* `proofsystems`: list of proof systems. Each proof system should have
  a short unique id, which is displayed in pictures and URLs, and a
  longer, properly written name. Optionally add notes and a list of
  other proof systems this proof system is a subsystem of (immediately
  following from the definition). e.g.
```
  res:
    longname: "Resolution"
    subsystemof:
      - ac0frege
```

* `formulas`: list of formulas. Each formula should have a short
  unique id, which is displayed in pictures and URLs, and a longer,
  properly written name. Optionally add notes and a list of other
  formulas that are contained in this formula. e.g.
```
  fphp:
    longname: "Pigeonhole principle with functionality axioms"
    extends: php
```

* `upperbounds`: upper bounds on the size of proving a formula with a
  proof system. It is indexed by proof system, and each proof system
  contains a list of formulas. Optionally add the size and a
  reference (see later). e.g.
```
  cp:
    tseitin:
      size: quasipoly
      source: DT20
```

* `lowerbounds`: like upperbounds.

* `simulations`: It is indexed by the "stronger" proof system, and
  each proof system contains a list of "weaker" proof systems it
  simulates. Optionally add the overhead and a reference. e.g.
```
  cp:
    unarysp:
      overhead: quasipoly
      source: FGIPRTW21
```

* `papers`: list of references used elsewhere, sorted by year. A DBLP
  key is strongly recommended. It can be obtained from DBLP by
  hovering over the "export record" button (2nd from the left); the
  last entry is the dblp key. e.g.
```
  CR79:
    dblp: journals/jsyml/CookR79
```

And what about separations? Just add a witnessing formula and the
corresponding upper and lower bounds.
